<?php

class DateModPlugin extends \RainLoop\Plugins\AbstractPlugin
{
	public function Init()
	{
		$this->addHook('filter.build-message', 'ManipulateDateHeader');
	}

	/**
	 * @param \MailSo\Mime\Message $oMessage
	 */
	public function ManipulateDateHeader(&$oMessage)
	{
		if ($oMessage instanceof \MailSo\Mime\Message)
		{
			$hour = date("H") - (int) $this->Config()->Get('plugin', 'mod_hours', 0);
			$minute = date("i") - (int) $this->Config()->Get('plugin', 'mod_minutes', 0);
			$second = date("s");
			$month = date("n") - (int) $this->Config()->Get('plugin', 'mod_months', 0);
			$day = date("j") - (int) $this->Config()->Get('plugin', 'mod_days', 0);
			$year = date("Y") - (int) $this->Config()->Get('plugin', 'mod_years', 0);
			$oMessage->SetDate(mktime($hour, $minute, $second, $month, $day, $year));
		}
	}

	public function configMapping()
	{
		return array(
			\RainLoop\Plugins\Property::NewInstance('mod_hours')
				->SetLabel('Hours')
				->SetType(\RainLoop\Enumerations\PluginPropertyType::INT)
				->SetDescription('Hours subtracted from the date header (sent time).')
				->SetDefaultValue(0),
			\RainLoop\Plugins\Property::NewInstance('mod_minutes')
				->SetLabel('Minutes')
				->SetType(\RainLoop\Enumerations\PluginPropertyType::INT)
				->SetDescription('Minutes subtracted from the date header (sent time).')
				->SetDefaultValue(0),
			\RainLoop\Plugins\Property::NewInstance('mod_days')
				->SetLabel('Days')
				->SetType(\RainLoop\Enumerations\PluginPropertyType::INT)
				->SetDescription('Days subtracted from the date header (sent time).')
				->SetDefaultValue(0),
			\RainLoop\Plugins\Property::NewInstance('mod_months')
				->SetLabel('Months')
				->SetType(\RainLoop\Enumerations\PluginPropertyType::INT)
				->SetDescription('Months subtracted from the date header (sent time).')
				->SetDefaultValue(0),
			\RainLoop\Plugins\Property::NewInstance('mod_years')
				->SetLabel('Years')
				->SetType(\RainLoop\Enumerations\PluginPropertyType::INT)
				->SetDescription('Years subtracted from the date header (sent time).')
				->SetDefaultValue(0)
		);
	}
}
