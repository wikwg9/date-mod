A simple [RainLoop](https://www.rainloop.net/) plugin to subtract given time from the email's date header.

## Installation
 - copy _'date-mod'_ folder into _'data/\_data\_/\_default\_/plugins'_ directory of your RainLoop installation
 - now you can enable and configure the plugin from your RainLoop admin panel
